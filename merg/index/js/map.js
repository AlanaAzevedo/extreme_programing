/*
 * 5 formas de personalizar a infowindow Google Maps
 * 2015 - www.marnoto.com
*/

// Variável que indica as coordenadas do centro do mapa
var center = new google.maps.LatLng(-29.698735, -53.518189);

// Variável que indica as coordenadas do marcador
var fabrica = new google.maps.LatLng(-29.698735, -53.518189);

// Função de inicialização do mapa
function initialize() {
  var mapOptions = {
    center: center,
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);

  // Variável que define o conteúdo da Info Window
  var conteudo = '<div id="iw-container">' +
                    '<div class="iw-title">Code Race - Recanto Maestro</div>' +
                    '<div class="iw-content">' +
                      '<div id="HY" class="iw-subTitle">História</div>' +
                      '<img src="images/vistalegre.jpg" alt="Antonio Menghti Faculdade" height="115" width="83">' +
                      '<p id+"HY"<font color="black">Campeonato de Programação - 12hrs</p> <a href="C:/Users/Simone/Desktop/Merg/html5up-strata/index.html" class="button next scrolly">AVANÇAR</a>'+                      
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';

  // Cria a nova Info Window com referência à variável infowindow e atribui o conteúdo
  var infowindow = new google.maps.InfoWindow({
    content: conteudo,

    // Atribuir um valor máximo para a largura da infowindow permite
    // maior controlo sobre os vários elementos do conteúdo
    maxWidth: 350
  });
   
  // Variável que define as opções do marcador
  var marker = new google.maps.Marker({
    position: fabrica,
    map: map,
    title:"Fábrica de Porcelana da Vista Alegre"
  });

  // Procedimento que mostra a Info Window através de um click no marcador
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker); // map e marker são as variáveis definidas anteriormente
  });

  // Evento que fecha a infoWindow com click no mapa
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });

  // *
  // INICIO DA PERSONALIZAÇÃO DA INFOWINDOW.
  // O evento google.maps.event.addListener() espera pela
  // criação da estrutura HTML da infowindow 'domready'
  // e antes da abertura da infowindow serão aplicados
  // os estilos definidos
  // *
  google.maps.event.addListener(infowindow, 'domready', function() {
  
    // Referência ao DIV que agrupa o fundo da infowindow
    var iwOuter = $('.gm-style-iw');

    /* Uma vez que o div pretendido está numa posição anterior ao div .gm-style-iw.
    * Recorremos ao jQuery e criamos uma variável iwBackground,
    * e aproveitamos a referência já existente do .gm-style-iw para obter o div anterior com .prev().
    */
    var iwBackground = iwOuter.prev();

    // Remover o div da sombra do fundo
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    // Remover o div de fundo branco
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    // Desloca a infowindow 115px para a direita
    iwOuter.parent().parent().css({left: '115px'});

    // Desloca a sombra da seta a 76px da margem esquerda 
    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Desloca a seta a 76px da margem esquerda 
    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Altera a cor desejada para a sombra da cauda
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

    // Referência ao DIV que agrupa os elementos do botão fechar
    var iwCloseBtn = iwOuter.next();

    // Aplica o efeito desejado ao botão fechar
    iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

    // Se o conteúdo da infowindow não ultrapassar a altura máxima definida, então o gradiente é removido.
    if($('.iw-content').height() < 140){
      $('.iw-bottom-gradient').css({display: 'none'});
    }

    // A API aplica automaticamente 0.7 de opacidade ao botão após o evento mouseout. Esta função reverte esse evento para o valor desejado.
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
    });
  });
}
google.maps.event.addDomListener(window, 'load', initialize);